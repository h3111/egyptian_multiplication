{-|
  egyptian_multiplication.hs

  Usage:    egyptian_multiplication { MULTIPLIER MULTIPLICAND } | { NUM-TESTS }

  Examples: egyptian_multiplication 5 3
            egyptian_multiplication 1000

  Compile:  ghc --make -Wall egyptian_multiplication.hs
  Lint:     hlint -s egyptian_multiplication.hs

  Author:   haskell@wu6ch.de

  Inspired by:

  E. G. Haffner: "Informatik - Das Lehrbuch fuer Dummies"
  chapter 51: "Logik und Korrektheit fuer Informatiker",
  section "Korrektheit von Programmen":
-}


import Control.Monad (replicateM)
import Control.Monad.State (State, execState, get, modify)
import Data.Bifunctor (bimap)
import Data.Bool (bool)
import System.Environment (getArgs)
import System.Random (randomRIO)
import Text.Read (readMaybe)


type Factors        = (Integer, Integer)
type DoubledHalved  = (Integer, Integer)
type FuncNameResult = (String,  Integer)


-- Auxiliaries:


toAbs :: Factors -> Factors
toAbs (i, j)
  | jAbs > iAbs = (jAbs, iAbs)
  | otherwise   = (iAbs, jAbs)
  where
    iAbs = abs i
    jAbs = abs j


adjustSign :: Factors -> Integer -> Integer
adjustSign (i, j) value
  | i < 0 && j >= 0 || i >= 0 && j < 0 = negate value
  | otherwise                          = value


-- Recursive with accumulator function:


accumulate :: Factors -> Integer -> Integer
accumulate (i, j) acc
  | odd j     = acc + i
  | otherwise = acc


accMult :: Factors -> Integer -> Integer
accMult (i, j) acc
  | j > 0     = accMult (i * 2, j `div` 2) $ accumulate (i, j) acc
  | otherwise = acc


accMult' :: Factors -> Integer
accMult' factors = adjustSign factors $ accMult (toAbs factors) 0


-- Recursive with state:


stateMult :: Factors -> State Integer Integer

stateMult (_, 0) = get

stateMult (i, j) = do
  modify (\acc -> if odd j then acc + i else acc)
  stateMult (i * 2, j `div` 2)

stateMult' :: Factors -> Integer
stateMult' factors = adjustSign factors $ execState (stateMult $ toAbs factors) 0


-- Fold:


makeDoubledList :: Integer -> [Integer]
makeDoubledList = iterate (*2)


makeHalvedList :: Integer -> [Integer]
makeHalvedList j = takeWhile (> 0) $ iterate (`div` 2) j


makeDoubledHalvedPairs :: Factors -> [DoubledHalved]
makeDoubledHalvedPairs (i, j) = zip (makeDoubledList i) (makeHalvedList j)


foldMult :: Factors -> Integer
foldMult factors = foldl foldFunc 0 $ makeDoubledHalvedPairs factors
  where
    foldFunc :: Integer -> DoubledHalved -> Integer
    foldFunc acc (d, h) = if odd h then acc + d else acc


foldMult' :: Factors -> Integer
foldMult' factors = adjustSign factors $ foldMult $ toAbs factors


-- Results:


showRefResult :: Factors -> String
showRefResult factors = mconcat [i, " * ", j, " = ", result]
  where
    (i, j) = bimap show show factors
    result = show $ uncurry (*) factors


showResults :: Factors -> [FuncNameResult] -> String
showResults factors namesResults =
  unlines $ showRefResult factors : (showResult <$> namesResults)
  where
    showResult :: FuncNameResult -> String
    showResult (n, r) = mconcat ["  ", n, show r]


showMult :: Bool -> Factors -> String
showMult p factors =
  if all (== uncurry (*) factors) results
    then bool "" (showRefResult factors <> "\n") p
    else showResults factors $ zip names results
  where
    names   = ["accMult   : ", "stateMult : ", "foldMult  : "]
    results = [accMult', stateMult', foldMult'] <*> pure factors


-- Test:


genTestPair :: IO Factors
genTestPair = do
  let testRange = (toInteger (minBound :: Int), toInteger (maxBound :: Int))
  i <- randomRIO testRange
  j <- randomRIO testRange
  return (i, j)


genTestPairList :: Int -> IO [Factors]
genTestPairList numTests = replicateM numTests genTestPair


testMult :: Int -> IO ()
testMult numTests = do
  testPairList <- genTestPairList numTests
  putStr $ mconcat [show numTests, " test", bool "s: " ": " (numTests == 1)]
  let failedList = filter (not . null) $ map (showMult False) testPairList
  if null failedList
    then putStrLn "all passed"
    else putStr $ "at least 1 failed!\n" <> head failedList


-- Main:


main :: IO ()
main = do
  let usage = "egyptian_multiplication { MULTIPLIER MULTIPLICAND } | { NUM-TESTS }"
  args <- getArgs
  case readMaybe <$> args of
    (Just i:Just j:_) -> putStr $ showMult True (i, j)
    (Just n:_)        -> testMult $ fromInteger n
    _                 -> putStrLn $ "Usage: " <> usage


-- EOF
