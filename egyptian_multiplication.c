/*
  egyptian_multiplication.c

  Based on:
    E. G. Haffner: "Informatik - Das Lehrbuch fuer Dummies"
    chapter 51: "Logik und Korrektheit fuer Informatiker",
    section "Korrektheit von Programmen"

  Compile:
    $ gcc -Wall -Wpedantic -o egyptian_multiplication_c egyptian_multiplication.c

  Lint:
    $ splint egyptian_multiplication.c  # see also './.splintrc'

  Execute (example):
    $ ./egyptian_multiplication_c 5 3
    5 * 3 = 15
*/


#include <stdio.h>    // printf
#include <stdlib.h>   // labs, atol


static long adjustSign( long, long, long ) /*@*/ ;  // pure function
static long mult(       long, long       ) /*@*/ ;  // pure function
static long mult_prime( long, long       ) /*@*/ ;  // pure function

int main( int, char** );


long adjustSign( long a, long b, long value)
{
  return (a < 0 && b >= 0) || (a >= 0 && b < 0) ? -1L * value : value;
}


long mult( long a, long b )
{
  long i = a;
  long j = b;

  long acc = 0;

  while ( j > 0 )
  {
    if ( j % 2 != 0 ) acc += i;

    i *= 2;
    j /= 2;
  }

  return acc;
}


long mult_prime( long a, long b )
{
  return adjustSign( a, b, mult( labs( a ), labs( b ) ) );
}


int main( int argc, char* argv [] )
{
  if ( argc >= 3 )
  {
    long a = atol( argv[ 1 ] );
    long b = atol( argv[ 2 ] );

    printf( "%ld * %ld = %ld\n", a, b, mult_prime( a, b ) );
  }
  else
  {
    printf( "Usage: egyptian_multiplication MULTIPLIER MULTIPLICAND\n" );
  }

  return 0;
}


// EOF
