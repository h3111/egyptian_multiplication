# _Egyptian multiplication_


## General

Recently, I found the pseudo-code for the _Egyptian multiplication_ in E. G.
Haffner's book _Informatik - Das Lehrbuch für Dummies_ in chapter 51: _Logik und
Korrektheit für Informatiker_, section _Korrektheit von Programmen_.

The _Egyptian multiplication_ is described here:
[_Russische Bauernmultiplikation_](https://de.wikipedia.org/wiki/Russische_Bauernmultiplikation)
(German) and here:
[_Egyptian multiplication_](https://en.wikipedia.org/wiki/Ancient_Egyptian_multiplication).


## _Egyptian multiplication_ in Haskell


### Compiling and Linting

Since `egyptian_multiplication.hs` does not use "special" modules, the
compilation should be possible with the standard Haskell installation:

````bash
$ sudo apt install haskell-platform hlint
$ ghc --make -Wall egyptian_multiplication.hs
````

Lint:

````bash
$ hlint -s egyptian_multiplication.hs
````


### Usage

Call `egyptian_multiplication` this way:

````bash
$ egyptian_multiplication { MULTIPLIER MULTIPLICAND } | { NUM-TESTS }
````

Examples:

````bash
$ ./egyptian_multiplication 5 3
5 * 3 = 15

$ ./egyptian_multiplication 10000
10000 tests: all passed
````


### Implementation hints

`egyptian_multiplication.hs` implements three variants of the _Egyptian
Multiplication_:

1.  _Recursive with accumulator function:_ The function `accMult` takes the
    accumulator value as an additional argument and is called recursively, while
    the multiplicand is greater than 0.

2.  _Recursive with state:_ The function `stateMult` holds the accumulator value
    as `State` and modifies this (global) state, while the multiplicand is
    greater than 0.

3.  _Fold:_ The function `foldMult` holds a list of pairs, where the first pair
    element is the doubled multiplier and the second pair element is the halved
    multiplicand. The number of list elements is the number of multiplicands
    down to 0 (not included). The fold function folds the list of pairs with
    the accumulator value.

The function `toAbs` takes a `Factors` type (a pair of `Integer`) and returns
a `Factors` type, where both factors are non-negative and the multiplier (the
first pair element) is always greater than (or at least equal to) the
multiplicand (the second pair element). (Can you image, why this is necessary or
useful, respectively?)

The function `adjustSign` takes a `Factors` type and an `Integer` value and
returns this `Integer` value: negative, if the `Factor` signs differ, otherwise
positive.

The `testMult` function uses functions `genTestPair` and `genTestPairList` to
generate a `Factor` list with random values. It maps all variants over this list
(using function `showMult`), checks the results for being correct, and outputs
the first incorrect calculation, if any.


## Annex: _Egyptian multiplication_ in C

In `egyptian_multiplication.c`, you find the _Egyptian multiplication_,
implemented in C.

Watch that the type _long_ has an upper bound. Therefore, overflows may occur.
I did not implement a check for the multiplier always greater than (or at least
equal to) the multiplicand. Also, the test functionality is missing.

Compile and lint `egyptian_multiplication.c` as follows:

````bash
$ gcc -Wall -Wpedantic -o egyptian_multiplication_c egyptian_multiplication.c
$ splint egyptian_multiplication.c  # see also './.splintrc'
````

Execute `egyptian_multiplication_c` as in the following example:

````bash
$ ./egyptian_multiplication_c 5 3
5 * 3 = 15
````


## Further links:

* https://mathspp.com/blog/egyptian-multiplication
* https://github.com/RojerGS/projects/blob/master/misc/egyptianMult.hs
* https://www.rosettacode.org/wiki/Ethiopian_multiplication#Haskell


<sub>[Wolfgang](mailto:haskell@wu6ch.de)</sub>


<!-- EOF -->
